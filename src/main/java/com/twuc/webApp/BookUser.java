package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/users")
public class BookUser {
    @GetMapping("/{id}/books")
    String getBookUser(@PathVariable int id) {
        return "UserId" + id;
    }

    @GetMapping("/uppercase/{ID}/books")
    String getBookUser_1(@PathVariable("ID") int id) {
        return "UserId" + id;
    }

    /* 2.3 */
    @GetMapping("/uppercase/{id}/books")
    String getBookUser_priority_1(@PathVariable int id) {
        return "UserId" + id;
    }

    @GetMapping("/uppercase/1/books")
    String getBookUser_priority_2() {
        return "userId";
    }

    /* 2.4.1 */
    @GetMapping("/?card/{id}")
    String getBookUser_wildCard_1(@PathVariable int id) {
        return "UserId" + id;
    }

    /* 2.4.2 */
    @GetMapping()
    String getBookUser_wildCard_2(@PathVariable int id) {
        return "UserId" + id;
    }

    /* 2.5.1 */
    @GetMapping("/wildcards/*")
    String getBookUser_wildCard_star_1() {
        return "UserId";
    }

    /* 2.5.2 */
    @GetMapping("/wildcards/*/books")
    String getBookUser_wildCard_star_2() {
        return "UserId";
    }

    /* 2.5.3 */
    @GetMapping("/wildcards/before/*/after")
    String getBookUser_wildCard_star_3() {
        return "UserId";
    }

    /* 2.6 */
    @GetMapping("/wildcards/**/after")
    String getBookUser_wildCard_multistars() {
        return "UserId";
    }

    /* 2.7 */
    @GetMapping("/regex/[a-zA-Z]{3}")
    String getBookUser_regex() {
        return "UserId";
    }


    /* 2.8.2 */
    @GetMapping("/query/string")
    String getBookUser_query_string_no_param(@RequestParam("id") int id) {
        return "UserId" + id;
    }

    /* 2.8.3 */
    @GetMapping(value = "/query", params = "!id")
    String getBookUser_query_string() {
        return "UserId";
    }


}


