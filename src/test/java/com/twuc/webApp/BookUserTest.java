package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookUserTest {
    @Test
    void should_return_request_user() {
        BookUser getBookUser = new BookUser();
        String result = getBookUser.getBookUser(2);
        assertEquals("UserId2", result);
    }

    @Test
    void should_return_request_wrong_user() {
        BookUser getBookUser = new BookUser();
        String result = getBookUser.getBookUser_1(2);
        assertEquals("UserId2", result);
    }
}
