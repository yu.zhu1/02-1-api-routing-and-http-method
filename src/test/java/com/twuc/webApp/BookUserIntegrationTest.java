package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class BookUserIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_and_get_user() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("UserId2"));
    }

    @Test
    void should_return_500_when_path_variable_mismatch_with_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/uppercase/1/books"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    /* 2.3 */
    @Test
    void should_return_200_when_path_variable_mismatch_with_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/uppercase/1/books"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("UserId1"));
    }
//    Response content expected:<UserId1> but was:<userId>
//    Expected :UserId1
//    Actual   :userId
//    Means hard-code edition is tested first

    /* 2.4.1 */
    @Test
    void should_return_200_when_wildCard_more_than_one_letter() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcard/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("UserId1"));
    }
//    Response status expected:<200> but was:<404>
//    Expected :200
//    Actual   :404
//    Means fail for more than one letter

    /* 2.4.2 */
    @Test
    void should_return_200_when_no_letter_inserted() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/noletterinsert"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("UserId1"));
    }
//    Response status expected:<200> but was:<404>
//    Expected :200
//    Actual   :404

    /* 2.5.1 */
    @Test
    void should_return_200_when_use_wildcard_star() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcards/lalala"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    /* 2.5.2 */
    @Test
    void should_return_200_when_use_wildcard_star_inside() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcards/anything/books"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    /* 2.5.3 */
    @Test
    void should_return_200_when_use_wildcard_star_inside_2() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcards/before/after"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
//    Response status expected:<200> but was:<404>
//    Expected :200
//    Actual   :404

    /* 2.6 */
    @Test
    void should_return_200_when_use_wildcard_multistars() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/wildcards/anything/after"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    /* 2.7 */
    @Test
    void should_return_200_when_use_regex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/regex/abc"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
//    Response status expected:<200> but was:<404>
//    Expected :200
//    Actual   :404


    /* 2.8.2 */
    @Test
    void should_return_200_when_no_param_provided() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/query/string"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

//    Response status expected:<200> but was:<400>
//    Expected :200
//    Actual   :400

    /* 2.8.3 */
    @Test
    void should_return_200_when_exclude_specific_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/query?id=2"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
//    Response status expected:<200> but was:<400>
//    Expected :200
//    Actual   :400

}